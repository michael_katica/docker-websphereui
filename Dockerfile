FROM tomcat:9.0-alpine
MAINTAINER Michael Katica <michael.katica@videa.tv>

ADD wmq-monitoring.war $CATALINA_HOME/webapps/wmq-monitoring.war
ADD tomcat-users.xml $CATALINA_HOME/conf/tomcat-users.xml